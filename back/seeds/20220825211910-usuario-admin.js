'use strict';

// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();

module.exports = {
  async up(queryInterface) {
    await queryInterface.bulkInsert(
      'usuario',
      [
        {
          email: process.env.USUARIO_ADMIN_EMAIL,
          nome: process.env.USUARIO_ADMIN_NOME,
          sobrenome: process.env.USUARIO_ADMIN_SOBRENOME,
        },
      ],
      {},
    );
  },

  async down(queryInterface) {
    await queryInterface.bulkDelete('usuario', null, {});
  },
};
