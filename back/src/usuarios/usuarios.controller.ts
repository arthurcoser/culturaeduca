import { Controller, Get, Param, ParseIntPipe } from '@nestjs/common';
import { UsuariosService } from './usuarios.service';

@Controller('usuarios')
export class UsuariosController {
  constructor(private readonly usuariosService: UsuariosService) {}

  /**
   * Rota para buscar usuário por id
   * @param usuarioId Id do usuário
   * @returns Usuário encontrado
   */
  @Get(':usuarioId')
  async busca(@Param('usuarioId', ParseIntPipe) usuarioId: number) {
    return this.usuariosService.busca(usuarioId);
  }

  /**
   * Rota para listar usuários
   * @returns Lista de usuários encontrados
   */
  @Get()
  async lista() {
    return this.usuariosService.lista();
  }
}
