import { getModelToken } from '@nestjs/sequelize';
import { Test, TestingModule } from '@nestjs/testing';
import { ModuleMocker, MockFunctionMetadata } from 'jest-mock';
import { NotFoundException } from '@nestjs/common';
import { Usuario } from '@/models/usuario.model';
import { UsuariosService } from './usuarios.service';

const moduleMocker = new ModuleMocker(global);

describe('UsuariosService', () => {
  let service: UsuariosService;

  // mock usuarioModel
  const usuarioModelMock = {
    findByPk: jest.fn().mockImplementation((id) => {
      if (id <= 0) return null;
      return { id };
    }),
    findAll: jest.fn().mockResolvedValue([]),
  };

  beforeEach(async () => {
    // test module
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsuariosService],
    })
      .useMocker((token) => {
        if (token === getModelToken(Usuario)) {
          return usuarioModelMock;
        }
        if (typeof token === 'function') {
          const mockMetadata = moduleMocker.getMetadata(
            token,
          ) as MockFunctionMetadata<any, any>;
          const Mock = moduleMocker.generateFromMetadata(mockMetadata);
          return new Mock();
        }
      })
      .compile();

    service = module.get<UsuariosService>(UsuariosService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  /**
   * busca(usuarioId: number)
   */

  it('busca usuário deve dar jogar NotFoundException para usuário não encontrado', async () => {
    const usuarioId = 0;
    try {
      await service.busca(usuarioId);
      throw new Error('Não deveria chegar aqui');
    } catch (err) {
      expect(usuarioModelMock.findByPk).toHaveBeenCalledWith(usuarioId);
      expect(err).toBeInstanceOf(NotFoundException);
      expect(err.message).toBe('Usuário não encontrado');
    }
  });

  it('busca usuário deve retornar um usuário para id existente', async () => {
    const usuarioId = 1;
    const usuario = await service.busca(usuarioId);
    expect(usuarioModelMock.findByPk).toHaveBeenCalledWith(usuarioId);
    expect(typeof usuario).toBe('object');
    expect(usuario.id).toBe(usuarioId);
  });

  /**
   * lista()
   */

  it('lista usuários deve retornar um array', async () => {
    const usuarios = await service.lista();
    expect(usuarioModelMock.findAll).toHaveBeenCalled();
    expect(Array.isArray(usuarios)).toBe(true);
  });
});
