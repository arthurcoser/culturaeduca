import { Test, TestingModule } from '@nestjs/testing';
import { UsuariosController } from './usuarios.controller';
import { UsuariosService } from './usuarios.service';
import { ModuleMocker, MockFunctionMetadata } from 'jest-mock';

const moduleMocker = new ModuleMocker(global);

describe('UsuariosController', () => {
  let controller: UsuariosController;

  // mock usuariosService
  const usuariosServiceMock = {
    busca: jest.fn(),
    lista: jest.fn(),
  };

  beforeEach(async () => {
    // test module
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsuariosController],
    })
      .useMocker((token) => {
        if (token === UsuariosService) {
          return usuariosServiceMock;
        }
        if (typeof token === 'function') {
          const mockMetadata = moduleMocker.getMetadata(
            token,
          ) as MockFunctionMetadata<any, any>;
          const Mock = moduleMocker.generateFromMetadata(mockMetadata);
          return new Mock();
        }
      })
      .compile();

    controller = module.get<UsuariosController>(UsuariosController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  /**
   * busca(usuarioId: number)
   */

  it('busca deve chamar o service de busca', async () => {
    const usuarioId = 1;
    await controller.busca(usuarioId);
    expect(usuariosServiceMock.busca).toHaveBeenCalledWith(usuarioId);
  });

  /**
   * lista()
   */

  it('lista deve chamar o service de busca', async () => {
    await controller.lista();
    expect(usuariosServiceMock.lista).toHaveBeenCalled();
  });
});
