import { Usuario } from '@/models/usuario.model';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';

@Injectable()
export class UsuariosService {
  constructor(@InjectModel(Usuario) private usuarioModel: typeof Usuario) {}

  /**
   * Busca usuário por id
   * @param usuarioId Id do usuário
   * @throws NotFoundException se usuário não for encontrado
   * @returns Usuário encontrado
   */
  async busca(usuarioId: number) {
    const usuario = await this.usuarioModel.findByPk(usuarioId);
    if (!usuario) {
      throw new NotFoundException('Usuário não encontrado');
    }
    return usuario;
  }

  /**
   * Lista todos os usuários
   * @returns Lista de usuários encontrados
   */
  async lista() {
    return this.usuarioModel.findAll();
  }
}
