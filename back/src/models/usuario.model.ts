import {
  AllowNull,
  AutoIncrement,
  Column,
  CreatedAt,
  DataType,
  IsEmail,
  Model,
  NotEmpty,
  PrimaryKey,
  Table,
  Unique,
  UpdatedAt,
} from 'sequelize-typescript';

@Table({ tableName: 'usuario', underscored: true })
export class Usuario extends Model<Usuario> {
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.INTEGER)
  id: number;

  @AllowNull(false)
  @Unique
  @IsEmail
  @Column(DataType.STRING)
  email: string;

  @AllowNull(false)
  @NotEmpty
  @Column(DataType.STRING)
  nome: string;

  @AllowNull(false)
  @NotEmpty
  @Column(DataType.STRING)
  sobrenome: string;

  @CreatedAt
  @Column(DataType.DATE)
  createdAt: Date;

  @UpdatedAt
  @Column(DataType.DATE)
  updatedAt: Date;
}
